package com.exadiawinds.breedingeggs;

import org.bukkit.Material;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Cow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.MushroomCow;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public final class BreedingEggsListener implements Listener {
		
	private static Plugin be = BreedingEggs.getInstance();

	@EventHandler
	public void creatureSpawn(CreatureSpawnEvent event) {
		// Check the event to see if it's a breeding event
		// If so, then we want to NOT breed a baby animal, but instead, breed a monster egg with that particular animal
		
		if(event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.BREEDING) {
			event.setCancelled(true);
			EntityType type = event.getEntityType();
			switch(type) {
				case CHICKEN:
					event.getLocation().getWorld().dropItemNaturally(event.getLocation(), new ItemStack(Material.MONSTER_EGG, 1, (short) 93));
					break;
				case COW:
					event.getLocation().getWorld().dropItemNaturally(event.getLocation(), new ItemStack(Material.MONSTER_EGG, 1, (short) 92));
					break;
				case HORSE:
					event.getLocation().getWorld().dropItemNaturally(event.getLocation(), new ItemStack(Material.MONSTER_EGG, 1, (short) 100));
					break;
				case MUSHROOM_COW:
					event.getLocation().getWorld().dropItemNaturally(event.getLocation(), new ItemStack(Material.MONSTER_EGG, 1, (short) 96));
					break;
				case OCELOT:
					event.getLocation().getWorld().dropItemNaturally(event.getLocation(), new ItemStack(Material.MONSTER_EGG, 1, (short) 98));
					break;
				case PIG:
					event.getLocation().getWorld().dropItemNaturally(event.getLocation(), new ItemStack(Material.MONSTER_EGG, 1, (short) 90));
					break;
				case SHEEP:
					event.getLocation().getWorld().dropItemNaturally(event.getLocation(), new ItemStack(Material.MONSTER_EGG, 1, (short) 91));
					break;
				case VILLAGER:
					event.getLocation().getWorld().dropItemNaturally(event.getLocation(), new ItemStack(Material.MONSTER_EGG, 1, (short) 120));
					break;
				case WOLF:
					event.getLocation().getWorld().dropItemNaturally(event.getLocation(), new ItemStack(Material.MONSTER_EGG, 1, (short) 95));
					break;
			}
		}
		
		// If a spawner egg is being dropped, we need to interrupt that event and have it drop a baby
		// mob instead of the full grown mob
		if (event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.SPAWNER_EGG) {
			EntityType type = event.getEntityType();
			be.getLogger().info(String.format("EntityType: %s", type));
			switch(type) {
			case CHICKEN:
				Chicken baby_chicken = (Chicken) event.getEntity();
				baby_chicken.setBaby();
				break;
			case COW:
				Cow baby_cow = (Cow) event.getEntity();
				baby_cow.setBaby();
				break;
			case HORSE:
				Horse baby_horse = (Horse) event.getEntity();
				baby_horse.setBaby();
				break;
			case MUSHROOM_COW:
				MushroomCow baby_mushroomcow = (MushroomCow) event.getEntity();
				baby_mushroomcow.setBaby();
				break;
			case OCELOT:
				Ocelot baby_ocelot = (Ocelot) event.getEntity();
				baby_ocelot.setBaby();
				break;
			case PIG:
				Pig baby_pig = (Pig) event.getEntity();
				baby_pig.setBaby();
				break;
			case SHEEP:
				Sheep baby_sheep = (Sheep) event.getEntity();
				baby_sheep.setBaby();
				break;
			case VILLAGER:
				Villager baby_villager = (Villager) event.getEntity();
				baby_villager.setBaby();
				break;
			case WOLF:
				Wolf baby_wolf = (Wolf) event.getEntity();
				baby_wolf.setBaby();
				break;
			}
		}
	}
}
