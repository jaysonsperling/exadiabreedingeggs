package com.exadiawinds.breedingeggs;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class BreedingEggs extends JavaPlugin {
	
	private static Plugin instance;

	public void onEnable() {
		instance = this;
		getServer().getPluginManager().registerEvents(new BreedingEggsListener(),  this);
		instance.getLogger().info("Created and maintained by jaysonsperling@gmail.com");
		instance.getLogger().info("CreatureSpawnEvent (2 handlers) enabled.");
	}
	
	public void onDisable() {
		// Nothing to do when we get disabled
		instance.getLogger().info("CreatureSpawnEvent (2 handlers) disabled.");
	}

	public static Plugin getInstance() {
		return instance;
	}
	
}
